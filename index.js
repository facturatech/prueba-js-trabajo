// Colocando las dependencias

const express = require('express');
const app = express();
const port = 5000;

// CORS PARA SOLICITUDES DENTRO DEL MISMO ENTORNO
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    next();
  });


// BASE DE DATOS // PARA AHORRAR TIEMPO //
const books = [
  { id: 1, title: 'Libro 1', author: 'Autor 1', year: 2001 },
  { id: 2, title: 'Libro 2', author: 'Autor 2', year: 2002 },
  { id: 3, title: 'Libro 3', author: 'Autor 3', year: 2003 },
  { id: 4, title: 'Libro 4', author: 'Autor 4', year: 2004 },
  { id: 5, title: 'Libro 5', author: 'Autor 5', year: 2005 },
  { id: 6, title: 'Libro 6', author: 'Autor 6', year: 2006 },
  { id: 7, title: 'Libro 7', author: 'Autor 7', year: 2007 },
  { id: 8, title: 'Libro 8', author: 'Autor 8', year: 2008 },
  { id: 9, title: 'Libro 9', author: 'Autor 9', year: 2009 },
  { id: 10, title: 'Libro 10', author: 'Autor 10', year: 2010 },
  { id: 11, title: 'Libro 11', author: 'Autor 11', year: 2011 },
  { id: 12, title: 'Libro 12', author: 'Autor 12', year: 2012 },
  { id: 13, title: 'Libro 13', author: 'Autor 13', year: 2013 },
  { id: 14, title: 'Libro 14', author: 'Autor 14', year: 2014 },
  { id: 15, title: 'Libro 15', author: 'Autor 15', year: 2015 },
  { id: 16, title: 'Libro 16', author: 'Autor 16', year: 2016 },
  { id: 17, title: 'Libro 17', author: 'Autor 17', year: 2017 },
  { id: 18, title: 'Libro 18', author: 'Autor 18', year: 2018 },
  { id: 19, title: 'Libro 19', author: 'Autor 19', year: 2019 },
  { id: 20, title: 'Libro 20', author: 'Autor 20', year: 2020 },

  
];
// CREANDO RUTA PRINCIPAL
app.get('/books', (req, res) => {
  res.json(books);
});

// CREANDO RUTA DE ID
app.get('/books/details/:id', (req, res) => {
  const bookId = parseInt(req.params.id);
  const book = books.find(book => book.id === bookId);

  // IF PARA ENVIAR JSON DE LIBRO O SI NO EXISTE
  if (book) {
    res.json(book);
  } else {
    res.status(404).json({ error: 'Libro no encontrado' });
  }
});

//MENSAJE AL INICIAS EL SCRIPT CON NODE// 

app.listen(port, () => {
  console.log(`Servidor API corriendo en http://localhost:${port}`);
});

